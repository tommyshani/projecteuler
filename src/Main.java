
public class Main extends ProjectEuler{

	public static void main(String[] args){
		
		ProjectEuler euler = new ProjectEuler();
		double sumOfSquares = euler.findSumOfSquare(100);
		double squareOfSums = euler.findSquareOfSum(100);
		double difference = euler.findDiff(sumOfSquares, squareOfSums);
		System.out.print("The difference is: " + difference);
		
	}
}

