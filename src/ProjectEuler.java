public class ProjectEuler {
	
	public double findSumOfSquare (double limit) {
		double squareOfSums = 0;
		for (double i = 1; i <= limit; i++) {
			sumOfSquares += i*i;
		}
		return sumOfSquares;
	}
	
	public double findSquareOfSum (double limit) {
		int sums = 0;
		for (double i = 0; i <= limit; i++) {
			sums += i;
		}
		double squareOfSums = Math.pow(sums, 2);
		return squareOfSums;
	}
	
	public double findDiff (double x, double y){ 
		double diff = y - x; 
		return diff;
	}
	
}
